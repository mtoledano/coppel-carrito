from django.db import models
from shop.models import Product
# Create your models here.

class Order(models.Model):
    first_name = models.CharField('Nombre(s)', max_length=150)
    last_name = models.CharField('Apellidos', max_length=150)
    email = models.EmailField()
    postal_code = models.CharField('Código Postal', max_length=15)
    adress = models.CharField('Dirección', max_length=120)
    city = models.CharField('Ciudad', max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    paid = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created_date', )

    def __str__(self):
        return 'Orden {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())

class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items')
    product = models.ForeignKey(Product, related_name='order_items')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity
